@extends('layout.user')

@section('aktif2')
class="active"
@endsection
@section('content')
<div class="col-xs-12">
               <A href="form-pemesanan"><button class="btn btn-warning"><i class="fa fa-plus"></i>  Tambah Pemesanan</button></a>
                <br><br>
                <?php 
                if (Session::has('success-message'))
                {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-check"></i> 
                    Pesanan baru telah berhasil ditambahkan.
                </div>
                <?php
                }
                ?>
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">DAFTAR PESANAN SAYA</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-hover">
                   <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal Pemesanan</th>
                        <th>Status Pesanan</th>
                        
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>15 Desember 1992</td>
                        <td><span class="label label-danger">Denied</span></td>
                      
                        <td><a href="detail-pesanan/1"><button class="btn btn-block btn-primary btn-sm">Detail</button></a></td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>15 Januari 1991</td>
                        <td><span class="label label-warning">Pending</span></td>
                        
                        <td><a href="detail-pesanan/1"><button class="btn btn-block btn-primary btn-sm">Detail</button></a></td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>1 Januari 1989</td>
                        <td><span class="label label-success">Approved</span></td>
                        
                        <td><a href="detail-pesanan/1"><button class="btn btn-block btn-primary btn-sm">Detail</button></a></td>
                      </tr>
                     
                      
                      
                     
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Tanggal Pemesanan</th>
                        <th>Status Pesanan</th>
                        
                        <th>Aksi</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

@endsection