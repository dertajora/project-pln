@extends('layout.user')
@section('aktif2')
class="active"
@endsection
@section('content')
@section('content')
<!-- Main content -->
        <section class="invoice">
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                <i class="fa fa-globe"></i> Konfirmasi Pemesanan
                <small class="pull-right">Tanggal Pemesanan: 3/3/2016</small>
              </h2>
            </div><!-- /.col -->
          </div>
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              Pemesan :
              <address>
                <strong>Derta Isyajora Rakhman</strong><br>
                
                Kontak: 085742724990<br>
                Email: derta.isyajora@gmail.com
              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
              Lokasi :
              <address>
                <strong>Desa Terindah di Lombok</strong><br>
                Kecamatan Penuh Kenangan
              </address>
            </div><!-- /.col -->

           
          </div><!-- /.row -->

          <!-- Table row -->
          <center>
          <div class="row">
            <div class="col-xs-3">
            </div>
            <div class="col-xs-6 table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>#OPSI PESANAN</th>
                    <th>DETAIL</th>
                    
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Jenis Lampu</td>
                    <td><B>MERKURY</B></td>
                    
                  </tr>
                  <tr>
                    <td>Tipe Lampu</td>
                    <td><B>GAS</B></td>
                    
                  </tr>
                  
                  <tr>
                    <td>Daya Lampu</td>
                    <td><B>PIJAR 51-100</B></td>
                    
                  </tr>
                  <tr>
                    <td>Kelas Lampu</td>
                    <td><B>1</B></td>
                    
                  </tr>
                  <tr>
                    <td>Jumlah Lampu</td>
                    <td><B>12</B></td>
                    
                  </tr>
                  <tr>
                    <td>Harga </td>
                    <td><B>50.000</B></td>
                    
                  </tr>
                  
                </tbody>
              </table>
            </div><!-- /.col -->
          </div><!-- /.row -->
          </center>

         
<div class="box box-primary">
               
                
                {{ Form::open(array('route'=>'simpan-pesanan', 'method' => 'POST','role' => 'form','class' => 'form')); }}


                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button class="btn btn-default"><a href="form-pemesanan">Cancel</a></button>
                    
                  </div>
                </form>
              </div><!-- /.box -->
@endsection