@extends('layout.user')
@section('aktif2')
class="active"
@endsection
@section('content')
@section('content')
<div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Formulir Pemesanan Lampu</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {{ Form::open(array('route'=>'konfirmasi-pesanan', 'method' => 'POST','role' => 'form','class' => 'form')); }}

                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Kecamatan</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Kangen Jogja ?">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Desa</label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Sleman">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">No Gardu</label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="34">
                    </div>

                    <div class="form-group">
                    <div class="row">
                    
                    <div class="col-xs-6">
                      
                      <label>Jenis Lampu</label>
                      <select class="form-control">
                      	<option>...</option>
                        <option>MERKURY</option>
                        <option>MERKURI</option>
                        <option>SOROT</option>
                        <option>TL</option>
                        <option>ML</option>
                        <option>SODIUM</option>
                        <option>LHE</option>
                        <option>SON-T</option>
                        <option>LED</option>
                        <option>DOP</option>
                        <option>PIJAR</option>
                      </select>
                    
                    </div>
                    <div class="col-xs-6">
                       <label>Tipe Lampu</label>
                      <select class="form-control">
                      	<option>...</option>
                        <option>GAS</option>
                        <option>PIJAR</option>
                      
                      </select>
                    </div>
                    </div>
                    </div>

                    <div class="form-group">
                    <div class="row">
                    
                    <div class="col-xs-6">
                       <label>Daya Lampu</label>
                      <select class="form-control">
                      	<option>...</option>
                        <option>PIJAR 25-50</option>
                        <option>PIJAR 51-100</option>
                        <option>PIJAR 101-200</option>
                        <option>PIJAR 201-300</option>
                        <option>PIJAR 301-400</option>
                        <option>PIJAR 401-500</option>
                        <option>PIJAR 501-600</option>
                        <option>PIJAR 601-700</option>
                        <option>PIJAR 701-800</option>
                        <option>PIJAR 801-900</option>
                        <option>PIJAR 901-1000</option>
                        <option>PELEPAS GAS 10-50</option>
                        <option>PELEPAS GAS 51-100</option>
                        <option>PELEPAS GAS 101-250</option>
                        <option>PELEPAS GAS 251-500</option>
                        <option>PELEPAS GAS 501-1000</option>
                      </select>
                    </div>
                    <div class="col-xs-6">
                      <label>Kelas Lampu</label>
                      <select class="form-control">
                      	<option>...</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                        <option>10</option>
                        <option>11</option>
                        <option>12</option>
                        <option>13</option>
                        <option>14</option>
                        <option>15</option>
                        <option>16</option>
                      </select>
                    </div>
                    </div>
                    </div>

                    <div class="form-group">
                    <div class="row">
                    
                    <div class="col-xs-6">
                      <label for="exampleInputPassword1">Jumlah Lampu</label>
                      <input type="text" class="form-control" placeholder="12">
                    </div>
                    <div class="col-xs-6">
                      <label for="exampleInputPassword1">Tarif/Bulan </label>
                      <input type="text" class="form-control" placeholder="50.000">
                    </div>
                    </div>
                    </div>


                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Pemesan</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Isikan nama anda...">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Kontak</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Isikan nomor HP/telepon anda...">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Isikan email anda...">
                    </div>
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    {{ Form::submit('Next', array('class' => 'btn btn-success', 'id' => 'login-button'));}}
                    
                     <button class="btn btn-default"><a href="pesanansaya">Cancel</a></button>
                  </div>
                </form>
              </div><!-- /.box -->
@endsection