@extends('layout.data')

@section('aktif2')
class="active"
@endsection
@section('content')
<div class="col-xs-12">
               <?php 
                if (Session::has('success-message'))
                {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-check"></i> 
                    Pesanan baru telah berhasil diterima.
                </div>
                
                <?php
                }
                ?>
                 <?php 
                if (Session::has('fail-message'))
                {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-check"></i> 
                    Pesanan baru telah berhasil ditolak.
                </div>
                
                <?php
                }
                ?>

                <br>
              <div class="box">

                <div class="box-header">
                  <h3 class="box-title">DAFTAR PESANAN BARU</h3>
                </div><!-- /.box-header -->
                 <div class="box-body">
                  <table id="example1" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal Pemesanan</th>
                        <th>Nama Pemesan</th>
                        <th>Kontak</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>15 Desember 1992</td>
                        <td>Rezan</td>
                        <td> 08121313242</td>
                        <td><a href="review-pesanan/1"><button class="btn btn-block btn-primary btn-sm">Review</button></a></td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>15 Januari 1991</td>
                        <td>Mona</td>
                        <td> 0814313242</td>
                        <td><a href="review-pesanan/1"><button class="btn btn-block btn-primary btn-sm">Review</button></a></td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>1 Januari 1989</td>
                        <td>Faisal</td>
                        <td> 081431324211</td>
                        <td><a href="review-pesanan/1"><button class="btn btn-block btn-primary btn-sm">Review</button></a></td>
                      </tr>
                     
                      
                      
                     
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Tanggal Pemesanan</th>
                        <th>Nama Pemesan</th>
                        <th>Kontak</th>
                        <th>Aksi</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

@endsection