@extends('layout.data')
@section('aktif1')
class="active"
@endsection
@section('content')
@section('content')
<div class="col-xs-12">
              <div class="row">
            <a href="pesananbaru">
            <div class="col-md-3 col-sm-6 col-xs-12">
              
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Pendaftaran Baru</span>
                  <span class="info-box-number">142</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->

            </div><!-- /.col -->
            </a>
          </div><!-- /.row -->
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Daftar Pemesanan</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal Pemesanan</th>
                        <th>Nama Pemesan</th>
                        <th>Kontak</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>15 Desember 1992</td>
                        <td>Rezan</td>
                        <td> 08121313242</td>
                        <td><a href="detail-pesanan-user/1"><button class="btn btn-block btn-primary btn-sm">Detail</button></a></td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>15 Januari 1991</td>
                        <td>Mona</td>
                        <td> 0814313242</td>
                        <td><a href="detail-pesanan-user/1"><button class="btn btn-block btn-primary btn-sm">Detail</button></a></td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>1 Januari 1989</td>
                        <td>Faisal</td>
                        <td> 081431324211</td>
                        <td><a href="detail-pesanan-user/1"><button class="btn btn-block btn-primary btn-sm">Detail</button></a></td>
                      </tr>
                     
                      
                      
                     
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Tanggal Pemesanan</th>
                        <th>Nama Pemesan</th>
                        <th>Kontak</th>
                        <th>Aksi</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

@endsection