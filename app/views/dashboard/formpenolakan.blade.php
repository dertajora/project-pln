@extends('layout.data')
@section('aktif2')
class="active"
@endsection
@section('content')
@section('content')
<!-- Main content -->



        <section class="invoice">
          <A href="{{ URL::To('review-pesanan/1') }}"><button class="btn btn-warning"><i class="fa"></i> Kembali</button></a>
                <br>
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                <i class="fa fa-globe"></i> Formulir Penolakan
                <small class="pull-right">Tanggal Pemesanan: 3/3/2016</small>
              </h2>
            </div><!-- /.col -->
          </div>
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              Pemesan :
              <address>
                <strong>Derta Isyajora Rakhman</strong><br>
                
                Kontak: 085742724990<br>
                Email: derta.isyajora@gmail.com
              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
              Lokasi :
              <address>
                <strong>Desa Terindah di Lombok</strong><br>
                Kecamatan Penuh Kenangan
              </address>
            </div><!-- /.col -->

           
          </div><!-- /.row -->

          <!-- Table row -->
         <div class="box box-warning">
                
                <div class="box-body">
                  {{ Form::open(array('route'=>'tolak-pesanan', 'method' => 'POST','role' => 'form','class' => 'form')); }}

                   
                    
                    <div class="form-group">
                      <label>Alasan</label>
                      <textarea class="form-control" rows="3" placeholder="Silahkan masukkan alasan..." ></textarea>
                    </div>

                    
                    
                    {{ Form::submit('Kirim', array('class' => 'btn btn-primary', 'id' => 'login-button'));}}
                    <button class="btn btn-default"><a href="review-pesanan/1">Cancel</a></button>
                     
                  </form>
                </div><!-- /.box-body -->
            </div>
      
@endsection