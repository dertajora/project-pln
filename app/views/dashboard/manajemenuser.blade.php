@extends('layout.data')

@section('aktif3')
class="active"
@endsection
@section('content')
<div class="col-xs-12">
               <a href="tambahuser"><button class="btn btn-warning"><i class="fa fa-plus"></i>  &nbspTambah User</button></a><br><Br>
               <?php 
                if (Session::has('success-message'))
                {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-check"></i> 
                    User baru telah berhasil ditambahkan.
                </div>
                
                <?php
                }
                ?>

                <?php 
                if (Session::has('update-message'))
                {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-check"></i> 
                    Data user telah berhasil diubah.
                </div>
                
                <?php
                }
                ?>

                <?php 
                if (Session::has('delete-message'))
                {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-check"></i> 
                    Data user telah berhasil dihapus.
                </div>
                
                <?php
                }
                ?>
                
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">DAFTAR USER</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama User</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>Derta Isyajora</td>
                        <td>dertajora</td>
                        <td> 08121313242</td>
                        <td><a href="edit-user/1"><button class="btn btn-warning btn-sm"><i class="fa fa-wrench"></i>&nbsp Ubah</button></a>
                          <A href="delete-user/1"><button class="btn  btn-danger btn-sm"><i class="fa fa-trash"></i>&nbsp Hapus</button></a></td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Rezansyah</td>
                        <td>rezan</td>
                        <td> 0814313242</td>
                        <td><a href="edit-user/1"><button class="btn btn-warning btn-sm"><i class="fa fa-wrench"></i>&nbsp Ubah</button></a>
                          <A href="delete-user/1"><button class="btn  btn-danger btn-sm"><i class="fa fa-trash"></i>&nbsp Hapus</button></a></td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Faical Arif</td>
                        <td>Faical</td>
                        <td> 081431324211</td>
                        <td><a href="edit-user/1"><button class="btn btn-warning btn-sm"><i class="fa fa-wrench"></i>&nbsp Ubah</button></a>
                          <A href="delete-user/1"><button class="btn  btn-danger btn-sm"><i class="fa fa-trash"></i>&nbsp Hapus</button></a></td>
                      </tr>
                     
                      
                      
                     
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Tanggal Pemesanan</th>
                        <th>Nama Pemesan</th>
                        <th>Kontak</th>
                        <th>Aksi</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

@endsection