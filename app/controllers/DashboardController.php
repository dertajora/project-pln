<?php

class DashboardController extends BaseController {

	public function getIndex(){

         return View::make('dashboard.pesanan'); 
    }

    public function getDaftarpesanan(){

         return View::make('dashboard.pesanan'); 
    }

    public function getPesananbaru(){

         return View::make('dashboard.pesananbaru'); 
    }

    public function getReviewPesanan($id){
        
         return View::make('dashboard.reviewpesanan'); 
    }

    public function getTerimaPesanan($id){
        
         return Redirect::to('pesananbaru')->with('success-message','success');
    }

    public function getFormPenolakan($id){
        
         return View::make('dashboard.formpenolakan'); 
    }

    public function postPenolakan(){
         
         return Redirect::to('pesananbaru')->with('fail-message','denied');
    }

    public function getDetailPesanan($id){
        
         return View::make('dashboard.detail'); 
    }

    #user
    public function getManajemenUser(){

         return View::make('dashboard.manajemenuser'); 
    }


    public function getTambahUser(){

         return View::make('dashboard.tambahuser'); 
    }

    public function postSimpanUser(){

         return Redirect::to('manajemen-user')->with('success-message','success');
    }

    public function getEditUser($id){

         return View::make('dashboard.edituser');
    }

    public function postUpdateUser(){

         return Redirect::to('manajemen-user')->with('update-message','success');
    }

    public function getDeleteUser($id){

         return Redirect::to('manajemen-user')->with('delete-message','success');
    }

   


    
  
	

      

}


	